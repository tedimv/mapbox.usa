import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { ControlsContainer, Label, OverlayButton, ResultCell, Row, ScrollArea } from './styled_html';
import { MapOverlays } from './index';
import { ApiClient } from '../../api/client';

const Controls = props => {
  const {
    baseZipCode,
    onChangeOverlay,
    activeOverlay,
    hasSetMarker
  } = props;

  const [ searchRadius, setSearchRadius ] = useState(1);
  const [ searchResults, setSearchResults ] = useState([]);
  const [ radiusUnit, setRadiusUnit ] = useState('km');
  const [ selectedCodes, setSelectedCodes ] = useState([]);

  function onChangeSearchRadius(e) {
    const value = e.target.value;
    if (isNaN(value)) return false;
    setSearchRadius(value);
  }

  async function getNearbyZipCodes() {
    try {
      const res = await ApiClient.findNearbyZipCodes(baseZipCode, searchRadius, radiusUnit)
      setSearchResults(res.data['zip_codes']);
    } catch (error) {
      debugger
      console.log({ error });
    }
  }

  return (
    <ControlsContainer>
      <Row>
        <OverlayButton
          isActive={activeOverlay === MapOverlays.ZIP_CODES}
          onClick={() => onChangeOverlay(prev => prev === MapOverlays.ZIP_CODES ? null : MapOverlays.ZIP_CODES)}>
          View ZipCodes
        </OverlayButton>
      </Row>

      <Row>
        <Label>
          Distance:
        </Label>
        <input
          style={{
            height: 30,
            display: 'inline-block',
            boxSizing: 'border-box'
          }}
          type='text'
          disabled={!hasSetMarker}
          value={searchRadius}
          onChange={onChangeSearchRadius}
        />
      </Row>

      <Row>
        <Label>
          Unit:
        </Label>
        <select
          disabled={!hasSetMarker}
          onChange={e => setRadiusUnit(e.target.value)}
          value={radiusUnit}>
          <option value='km'>Kilometers</option>
          <option value='miles'>Miles</option>
        </select>
      </Row>

      <Row>
        <button
          disabled={!hasSetMarker}
          onClick={getNearbyZipCodes}
          style={{
            gridColumn: 2
          }}>
          Get codes near marker
        </button>
      </Row>

      <ScrollArea>
        {baseZipCode &&
        <div
          style={{
            fontWeight: 400,
            color: '#444444',
            marginBottom: 5,
            borderBottom: '1px solid grey'
          }}>
          {baseZipCode}
        </div>}
        {searchResults.length ?
          <section style={{ display: 'flex' }}>
            <ResultCell/>
            <ResultCell>Zip code</ResultCell>
            <ResultCell>Distance</ResultCell>
            <ResultCell>City</ResultCell>
            <ResultCell>State</ResultCell>
          </section> : null}
        {searchResults.map((zipC, i) => {
          const cbId = `cb-${i}`;
          const isSelected = selectedCodes.includes(cbId);
          return (
            <div
              key={`result-${i}/${zipC.length}`}
              style={{ display: 'flex', alignItems: 'center' }}
            >
              <ResultCell>
                <input
                  type='checkbox'
                  checked={isSelected}
                  onChange={() => setSelectedCodes(prev => isSelected ? prev.filter(id => id !== cbId) : [ ...prev, cbId ])}
                />
              </ResultCell>
              <ResultCell>
                {zipC['zip_code']}
              </ResultCell>
              <ResultCell>
                {zipC.distance}
              </ResultCell>
              <ResultCell>
                {zipC.city}
              </ResultCell>
              <ResultCell>
                {zipC.state}
              </ResultCell>
            </div>
          )
        })}
      </ScrollArea>

    </ControlsContainer>
  );
}

Controls.propTypes = {
  baseZipCode: PropTypes.string.isRequired,
  onChangeOverlay: PropTypes.func.isRequired,
  activeOverlay: PropTypes.string,
  hasSetMarker: PropTypes.bool
};

export default Controls;
