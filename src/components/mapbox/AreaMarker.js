import React from 'react';
import PropTypes from 'prop-types';
import { Marker, Layer, Feature } from 'react-mapbox-gl';

import { MarkerIconImg } from './styled_html';

function AreaMarker(props) {
  const {
    markerCoordinates,
    onRemoveMarker
  } = props;

  return (
    <>
      <Layer
        lineCap='circle'
        linePaint={{
          'line-opacity': 1,
          'line-color': 'red',
          'line-width': 1,
        }}
      >
        <Feature coordinates={markerCoordinates}/>
      </Layer>

      <Marker
        onClick={onRemoveMarker}
        coordinates={markerCoordinates}
      >
        <MarkerIconImg/>
      </Marker>
    </>
  );
}

AreaMarker.propTypes = {
  markerCoordinates: PropTypes.arrayOf(PropTypes.number).isRequired,
  onRemoveMarker: PropTypes.func.isRequired
};

export default AreaMarker;
