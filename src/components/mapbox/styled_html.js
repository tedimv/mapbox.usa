import styled from 'styled-components';

import MarkerIcon from '../../assets/icons/marker.png';

export const ControlsContainer = styled.div`
  width: 320px;
  height: 500px;
  position: absolute;
  top: 30px;
  right: 30px;
  display: flex;
  flex-direction: column;
  background-color: #fff;
  background-opacity: 0.8;
  padding: 15px 10px 10px 10px;
`;

export const OverlayButton = styled.button`
  width: 100%;
  height: 30px;
  cursor: pointer;
  border: 1px solid ${({ isActive }) => isActive ? 'grey' : 'lightgrey'};
  background-color: ${({isActive}) => isActive ? 'lightgreen' : '#fff'};
  border-radius: 3px;
  grid-column: 2;
  margin-left: auto;
`;

export const MarkerIconImg = styled.div`
  height: 32px;
  width: 32px;
  background-image: url(${MarkerIcon});
  cursor: pointer;
`;

export const ScrollArea = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  overflow-x: hidden;
  border: 1px solid lightgrey;
  padding: 5px;
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: 0.2fr 0.8fr;
  grid-template-rows: 30px;
  margin-bottom: 15px;
`;

export const Label = styled.span`
  margin-right: 10px;
  display: flex;
  height: 100%;
  align-items: center;
`;

export const ResultCell = styled.div`
  width: 20%;
`
