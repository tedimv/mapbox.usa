import React, { useState } from 'react';
import ReactMapboxGl, { GeoJSONLayer } from 'react-mapbox-gl';

import zipCodesMap from '../../assets/geo_data/usa_zip_codes.json';
import Controls from './Controls';
import AreaMarker from './AreaMarker';


const Map = ReactMapboxGl({
  accessToken: 'pk.eyJ1IjoibWFwbW90YSIsImEiOiJja2kxazk3ZjcwYTJyMnBxbmNrdGVqOTZpIn0.zUy-8mtveeBEvfmLTeOGiQ',
});

const mapContainerStyles = {
  height: '100vh',
  width: '100vw',
  position: 'absolute',
  top: 0,
  left: 0
}

export const MapOverlays = {
  STATES: 'STATES',
  COUNTIES: 'COUNTIES',
  ZIP_CODES: 'ZIP_CODES'
}

const Mapbox = props => {

  const [ activeMapOverlay, setActiveMapOverlay ] = useState(MapOverlays.ZIP_CODES);
  const [ baseZipCode, setBaseZipCode ] = useState('');
  const [ markerCoordinates, setMarkerCoordinates ] = useState([]);
  const [ coordinates, setCoordinates ] = useState([ -100, 40 ]);
  const [ zoom, setZoom ] = useState([ 4.5 ]);


  function hoverSingleZipCode(hoverObj) {
    // Don't change last saved zip code if a marker has been placed
    if (markerCoordinates.length) return false;
    const singleCode = hoverObj.features[0].properties.ZCTA5CE10.toString();
    if (baseZipCode.toString() !== singleCode) {
      setBaseZipCode(singleCode);
    }
  }

  function syncMapCenter(map) {
    setCoordinates(Object.values(map.getCenter()))
  }

  function syncMapZoom(map) {
    setZoom([ map.getZoom() ]);
  }

  function setMarker(map, evt) {
    setMarkerCoordinates(Object.values(evt.lngLat))
  }

  function removeMarker() {
    setMarkerCoordinates([]);
  }


  return (
    <Map
      // eslint-disable-next-line react/style-prop-object
      style='mapbox://styles/mapbox/streets-v9'
      containerStyle={mapContainerStyles}
      center={coordinates}
      onDragEnd={syncMapCenter}
      zoom={zoom}
      onZoomEnd={syncMapZoom}
      onClick={setMarker}
    >

      {markerCoordinates.length &&
      <AreaMarker
        markerCoordinates={markerCoordinates}
        onRemoveMarker={removeMarker}
      />}

      {activeMapOverlay === MapOverlays.ZIP_CODES &&
      <GeoJSONLayer
        sourceLayer='zip-codes'
        data={zipCodesMap}
        center={coordinates}
        fillOnMouseMove={hoverSingleZipCode}
        fillPaint={{
          'fill-opacity': 0.3,
          'fill-color': '#b58605',
        }}
        linePaint={{
          'line-opacity': 1,
          'line-color': '#000',
          'line-width': 1,
        }}
      />}

      <Controls
        baseZipCode={baseZipCode}
        onChangeOverlay={setActiveMapOverlay}
        activeOverlay={activeMapOverlay}
        hasSetMarker={!!markerCoordinates.length}
      />
    </Map>
  );
}

export default Mapbox;
