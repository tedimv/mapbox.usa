import axios from 'axios';

// https://www.zipcodeapi.com/API
const apiKey = '2RBAj9N6JT4eKTRKZt68r59xuum0JqdKp3uZJ3De9uYwgV4T72bw6p7QLD03Ulwe';

const headers = {
  'Access-Control-Allow-Origin': '*',
}

export class ApiClient {
  static findNearbyZipCodes(zipCode, distance, unit = 'km') {
    return axios.get(`/rest/${apiKey}/radius.json/${zipCode}/${distance}/${unit}`, { headers });
  }
}
