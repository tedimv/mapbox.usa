import React from 'react';
import 'mapbox-gl/dist/mapbox-gl.css';

import Mapbox from './components/mapbox';

function App() {
  return (
    <div className="App">
      <Mapbox/>
    </div>
  );
}

export default App;
